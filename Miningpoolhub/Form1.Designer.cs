﻿namespace Miningpoolhub
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.getUserAllBalancesPanel1 = new Miningpoolhub.Control.GetUserAllBalancesPanel();
            this.SuspendLayout();
            // 
            // getUserAllBalancesPanel1
            // 
            this.getUserAllBalancesPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.getUserAllBalancesPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.getUserAllBalancesPanel1.Location = new System.Drawing.Point(0, 0);
            this.getUserAllBalancesPanel1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.getUserAllBalancesPanel1.Name = "getUserAllBalancesPanel1";
            this.getUserAllBalancesPanel1.Size = new System.Drawing.Size(744, 586);
            this.getUserAllBalancesPanel1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 586);
            this.Controls.Add(this.getUserAllBalancesPanel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BALANCES On Miningpolhub";
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private Control.GetUserAllBalancesPanel getUserAllBalancesPanel1;
    }
}

