﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miningpoolhub.Data
{
    public class Bids
    {

        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("volume")]
        public decimal Volume { get; set; }

        [JsonProperty("highbid")]
        public decimal Highbid { get; set; }
    }

    public class Asks
    {

        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("volume")]
        public decimal Volume { get; set; }

        [JsonProperty("highbid")]
        public decimal Highbid { get; set; }
    }

    public class Orderbook
    {

        [JsonProperty("bids")]
        public Bids Bids { get; set; }

        [JsonProperty("asks")]
        public Asks Asks { get; set; }
    }

    public class Coins
    {

        [JsonProperty("pairing_id")]
        public int PairingId { get; set; }

        [JsonProperty("primary_currency")]
        public string PrimaryCurrency { get; set; }

        [JsonProperty("secondary_currency")]
        public string SecondaryCurrency { get; set; }

        [JsonProperty("change")]
        public decimal Change { get; set; }

        [JsonProperty("last_price")]
        public decimal LastPrice { get; set; }

        [JsonProperty("volume_24hours")]
        public decimal Volume24hours { get; set; }

        [JsonProperty("orderbook")]
        public Orderbook Orderbook { get; set; }
    }

    public class BxMarket
    {

        [JsonProperty("1")]
        public Coins Coin1 { get; set; }

        [JsonProperty("2")]
        public Coins Coin2 { get; set; }

        [JsonProperty("3")]
        public Coins Coin3 { get; set; }

        [JsonProperty("4")]
        public Coins Coin4 { get; set; }

        [JsonProperty("5")]
        public Coins Coin5 { get; set; }

        [JsonProperty("6")]
        public Coins Coin6 { get; set; }

        [JsonProperty("7")]
        public Coins Coin7 { get; set; }

        [JsonProperty("8")]
        public Coins Coin8 { get; set; }

        [JsonProperty("9")]
        public Coins Coin9 { get; set; }

        [JsonProperty("10")]
        public Coins Coin10 { get; set; }

        [JsonProperty("11")]
        public Coins Coin11 { get; set; }

        //
        [JsonProperty("12")]
        public Coins Coin12 { get; set; }
       

        [JsonProperty("13")]
        public Coins Coin13 { get; set; }

        [JsonProperty("14")]
        public Coins Coin14 { get; set; }

        [JsonProperty("15")]
        public Coins Coin15 { get; set; }

        [JsonProperty("16")]
        public Coins Coin16 { get; set; }

        [JsonProperty("17")]
        public Coins Coin17 { get; set; }

        [JsonProperty("18")]
        public Coins Coin18 { get; set; }

        [JsonProperty("19")]
        public Coins Coin19 { get; set; }

        [JsonProperty("20")]
        public Coins Coin20 { get; set; }

        //
        [JsonProperty("21")]
        public Coins Coin21 { get; set; }

        [JsonProperty("22")]
        public Coins Coin22 { get; set; }

        [JsonProperty("23")]
        public Coins Coin23 { get; set; }

        [JsonProperty("24")]
        public Coins Coin24 { get; set; }

        [JsonProperty("25")]
        public Coins Coin25 { get; set; }

        [JsonProperty("26")]
        public Coins Coin26 { get; set; }

        [JsonProperty("27")]
        public Coins Coin27 { get; set; }

        [JsonProperty("28")]
        public Coins Coin28 { get; set; }

        [JsonProperty("29")]
        public Coins Coin29 { get; set; }

        [JsonProperty("30")]
        public Coins Coin30 { get; set; }
    }
}
    

   

    
