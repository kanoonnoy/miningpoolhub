﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miningpoolhub.Data
{
    public class Datum
    {
          
        [JsonProperty("coin")]
        public string Coin { get; set; }

        [JsonProperty("confirmed")]
        public decimal Confirmed { get; set; }

        [JsonProperty("unconfirmed")]
        public decimal Unconfirmed { get; set; }

        [JsonProperty("ae_confirmed")]
        public decimal AeConfirmed { get; set; }

        [JsonProperty("ae_unconfirmed")]
        public decimal AeUnconfirmed { get; set; }

        [JsonProperty("exchange")]
        public decimal Exchange { get; set; }
    }

    public class Getuserallbalances
    {

        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("runtime")]
        public double Runtime { get; set; }

        [JsonProperty("data")]
        public IList<Datum> Data { get; set; }
    }

    public class ApiMiningpoolhub
    {

        [JsonProperty("getuserallbalances")]
        public Getuserallbalances Getuserallbalances { get; set; }
    }


}
