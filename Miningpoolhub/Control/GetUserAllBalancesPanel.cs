﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace Miningpoolhub.Control
{
    public partial class GetUserAllBalancesPanel : UserControl
    {
        private DataTable _dtSource = new DataTable();
        private DataTable _dtView = new DataTable();

        private string _LinkTemplate = @"https://[<coin_name>.]miningpoolhub.com/index.php?page=api&action=<method>&api_key=<user_api_key>[&<argument>=<value>]";
        private string _LinkCoinMarketCap = @"https://api.coinmarketcap.com/v1/ticker/?convert=THB&limit=200";
        private string _LinkCoinBx = @"https://bx.in.th/api/";
        private string _Coin_name = "";
        private string _Argument = "ID";
        private string _ArgumentValue = "";
        private string _ApiKey = "";
        private string _Method = "getuserallbalances";

        private IList<Data.CoinMarketCap> _CoinMarketCap;
        private Data.BxMarket _BxMerket;
        public GetUserAllBalancesPanel()
        {
            InitializeComponent();
            InitialData();
        }
        
        private void InitialData()
        {
            try
            {
                this.label3.Text = string.Empty;
                _dtSource.Clear();

                _dtSource.Columns.Add("coin", typeof(string));
                _dtSource.Columns.Add("symbol", typeof(string));
                _dtSource.Columns.Add("confirmed", typeof(decimal));
                _dtSource.Columns.Add("unconfirmed", typeof(decimal));
                _dtSource.Columns.Add("ae_confirmed", typeof(decimal));
                _dtSource.Columns.Add("ae_unconfirmed", typeof(decimal));
                _dtSource.Columns.Add("exchange", typeof(decimal));


                this.dataGridView1.DataSource = _dtSource;

                this.dataGridView1.Columns["coin"].HeaderText = "Coin";
                this.dataGridView1.Columns["coin"].Width = 105;
                this.dataGridView1.Columns["coin"].DefaultCellStyle.BackColor = Color.FromArgb(255, 192, 255);

                this.dataGridView1.Columns["confirmed"].HeaderText = "Normal Wallet";
                this.dataGridView1.Columns["confirmed"].Width = 180;
                this.dataGridView1.Columns["confirmed"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
                this.dataGridView1.Columns["confirmed"].DefaultCellStyle.BackColor = Color.FromArgb(192, 255, 192);
               // this.dataGridView1.Columns["unconfirmed"].Visible = false;

                this.dataGridView1.Columns["ae_confirmed"].HeaderText = "Auto Exchange Wallet";
                this.dataGridView1.Columns["ae_confirmed"].Width = 180;
                this.dataGridView1.Columns["ae_confirmed"].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192);
                this.dataGridView1.Columns["ae_confirmed"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
               // this.dataGridView1.Columns["ae_unconfirmed"].Visible = false;
                this.dataGridView1.Columns["exchange"].HeaderText = "On Exchange";
                this.dataGridView1.Columns["exchange"].Width = 180;
                this.dataGridView1.Columns["exchange"].DefaultCellStyle.BackColor = Color.FromArgb(255, 224, 192);
                this.dataGridView1.Columns["exchange"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void Relink()
        {
            try
            {

                this.groupBox2.Enabled = false;
                this.Cursor = Cursors.WaitCursor;
                this.label9.Text = string.Empty;
                decimal bxPrice = 0;
                decimal CoinMarketPrice = 0;
                decimal Diff = 0;
                using (WebClient wc = new WebClient())
                {
                    var jsoninput = wc.DownloadString(_LinkCoinMarketCap);
                    _CoinMarketCap = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<Data.CoinMarketCap>>(jsoninput);

                    // Console.WriteLine(_CoinMarketCap.Name);
                    var result = _CoinMarketCap.First(x => x.Symbol == "ETH");

                    if (result != null)
                    {
                        var PriceChange = Convert.ToDecimal(result.PercentChange24h);
                        var price = Convert.ToDecimal(result.PriceThb);
                        var actor = PriceChange < 0 ? "-" : "+";
                        if (PriceChange == 0) actor = "";
                        CoinMarketPrice = price;
                        this.label4.Text = string.Format("{0}={1:N2}({2}{3:N2})", result.Symbol, price,actor, PriceChange);
                        
                    }

                    foreach (DataRow row in _dtSource.Rows)
                    {
                        var resultCheck = _CoinMarketCap.FirstOrDefault(x => x.Id.ToLower() == row["coin"].ToString());
                       if(resultCheck != null)
                        {
                            row["symbol"] = resultCheck.Symbol;
                            row["coin"] = resultCheck.Name;
                        }
                    }

                    _dtSource.AcceptChanges();
                }

                using (WebClient wc = new WebClient())
                {
                    var jsoninput = wc.DownloadString(_LinkCoinBx);
                    _BxMerket = Newtonsoft.Json.JsonConvert.DeserializeObject<Data.BxMarket>(jsoninput);

                    var actor = _BxMerket.Coin21.Change < 0 ? "-" : "+";
                    if (_BxMerket.Coin21.Change == 0) actor = "";
                    bxPrice = _BxMerket.Coin21.LastPrice;
                    this.label5.Text = string.Format("{0}={1:N2}({2}{3:N2})", _BxMerket.Coin21.SecondaryCurrency, _BxMerket.Coin21.LastPrice, actor,_BxMerket.Coin21.Change);

                    var rowview = this._dtSource.Select(string.Format("symbol='{0}'",_BxMerket.Coin21.SecondaryCurrency));
                    if (rowview != null)
                    {
                        if (rowview.Length > 0)
                        {
                            var t = (decimal)rowview[0]["confirmed"];
                            var sum = t * _BxMerket.Coin21.LastPrice;
                         
                            this.label9.Text = string.Format("{0}={1:N2}", _BxMerket.Coin21.SecondaryCurrency, sum);
                        }
                    }
                }

                if(_BxMerket != null)
                {
                    
                }

                Diff = Math.Abs(CoinMarketPrice - bxPrice);

                this.label11.Text = string.Format("{0:N2}",Diff);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.groupBox2.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }
        public void Reload()
        {
            try
            {
                this.Enabled = false;
                this.Cursor = Cursors.WaitCursor;
                if (_dtSource.Rows.Count > 0)
                    _dtSource.Rows.Clear();

                _ApiKey = this.ApitextBox.Text;
                _ArgumentValue = this.UserIDTextbox.Text;
                _Coin_name = !string.IsNullOrEmpty(_Coin_name) ? string.Format("{0}.", _Coin_name) : "";
                var argumentText = !string.IsNullOrEmpty(_Argument) && !string.IsNullOrEmpty(_ArgumentValue) ? string.Format("&{0}={1}", _Argument, _ArgumentValue) : "";

                var url = _LinkTemplate.Replace("[<coin_name>.]", _Coin_name)
                    .Replace("<method>",_Method)
                    .Replace("<user_api_key>", _ApiKey)
                    .Replace("[&<argument>=<value>]", argumentText)
                    ;

                Console.WriteLine(url);

                using (WebClient wc = new WebClient())
                {
                    var jsoninput = wc.DownloadString(url);

                    this.label3.Text = string.Format(this.label3.Tag.ToString(),DateTime.Now);
                    var json = Newtonsoft.Json.JsonConvert.DeserializeObject<Data.ApiMiningpoolhub>(jsoninput);

                    if(json != null)
                    {
                        foreach (var item in json.Getuserallbalances.Data)
                        {
                            var RowAdd = _dtSource.NewRow();

                            RowAdd["coin"] = item.Coin;
                            RowAdd["confirmed"] = item.Confirmed;
                            RowAdd["unconfirmed"] = item.Unconfirmed;
                            RowAdd["ae_confirmed"] = item.AeConfirmed;
                            RowAdd["ae_unconfirmed"] = item.AeUnconfirmed;
                            RowAdd["exchange"] = item.Exchange;

                            _dtSource.Rows.Add(RowAdd);
                        }
                    }
                }


                if(_dtSource.Rows.Count > 0)
                {
                    //this.dataGridView1.DataSource = _dtSource;
                }

                Relink();

                Application.DoEvents();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }
        private void btRefresh_Click(object sender, EventArgs e)
        {
            Reload();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btRelink_Click(object sender, EventArgs e)
        {
            Relink();
        }
    }
}
